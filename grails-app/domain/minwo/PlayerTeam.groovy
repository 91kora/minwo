package minwo

class PlayerTeam {
	Date entry
	Date exit
	Long goals
	
	Player player
	Team team
	
	
    static constraints = {
		entry nullable : false
		exit nullable : false
		goals min : 0l
		
    }
	
	String toString() {return entry!=null && exit!=null  ?entry.format("yyyy") + "-" + exit.format("yyyy") +" "+ player.toString()+" (" + team +")":""}
}
