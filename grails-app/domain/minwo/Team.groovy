package minwo

class Team {
	String teamName
	
	static hasMany = [teamPlayers: PlayerTeam]
	static mappedBy = [teamPlayers:"team"]
	
    static constraints = {
		teamPlayers nullable : true
		teamName blank: false
    }
	
	String toString() {return teamName}
}
