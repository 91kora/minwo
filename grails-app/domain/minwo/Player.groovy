package minwo

class Player {
	String firstName
	String lastName
	Date brithDate
	
	static hasMany = [teams: PlayerTeam]
	static mappedBy = [teams:"player"]
	
    static constraints = {
		teams nullable : true
		firstName balnk : false
		lastName blank : false
    }
	
	String toString() {return firstName + " " + lastName}
}
